package ru.t1.stepanishchev.tm.api.model;

import org.jetbrains.annotations.Nullable;

public interface ICommand {

    @Nullable
    String getArgument();

    @Nullable
    String getName();

    @Nullable
    String getDescription();

    void execute();

}