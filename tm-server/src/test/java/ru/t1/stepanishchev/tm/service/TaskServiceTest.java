package ru.t1.stepanishchev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.stepanishchev.tm.api.repository.ITaskRepository;
import ru.t1.stepanishchev.tm.api.service.ITaskService;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.enumerated.TaskSort;
import ru.t1.stepanishchev.tm.exception.entity.StatusEmptyException;
import ru.t1.stepanishchev.tm.exception.field.*;
import ru.t1.stepanishchev.tm.model.Task;
import ru.t1.stepanishchev.tm.repository.TaskRepository;

import java.util.List;
import java.util.UUID;

public final class TaskServiceTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final ITaskService service = new TaskService(repository);

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String taskName = UUID.randomUUID().toString();

    @NotNull
    private final String taskDescription = UUID.randomUUID().toString();

    @NotNull
    private final Status taskStatus = Status.COMPLETED;

    @Test
    public void createTask() {
        Assert.assertEquals(0, service.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, taskName));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, taskName, taskDescription));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null, taskDescription));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, taskName, null));
        @NotNull final Task task = service.create(userId, taskName);
        Assert.assertEquals(1, service.getSize(userId));
    }

    @Test
    public void updateById() {
        @NotNull final String newDescription = UUID.randomUUID().toString();
        @NotNull final String newName = UUID.randomUUID().toString();
        @NotNull final Task task = service.create(userId, taskName, taskDescription);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById(null, task.getId(), newName, newDescription));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(userId, task.getId(), null, newDescription));
        service.updateById(userId, task.getId(), newName, newDescription);
        Assert.assertEquals(newName, service.findOneById(userId, task.getId()).getName());
        Assert.assertEquals(newDescription, service.findOneById(userId, task.getId()).getDescription());
    }

    @Test
    public void updateByIndex() {
        @NotNull final String newDescription = UUID.randomUUID().toString();
        @NotNull final String newName = UUID.randomUUID().toString();
        service.create(userId, taskName, taskDescription);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex(null, 0, newName, newDescription));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.updateByIndex(userId, 0, null, newDescription));
        service.updateByIndex(userId, 0, newName, newDescription);
        Assert.assertEquals(newName, service.findOneByIndex(userId, 0).getName());
        Assert.assertEquals(newDescription, service.findOneByIndex(userId, 0).getDescription());
    }

    @Test
    public void changeTaskStatusByIndex() {
        service.create(userId, taskName, taskDescription);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeTaskStatusByIndex(null, 0, taskStatus));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.changeTaskStatusByIndex(userId, 1, taskStatus));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeTaskStatusByIndex(userId, 0, null));
        service.changeTaskStatusByIndex(userId, 0, taskStatus);
        Assert.assertEquals(service.findOneByIndex(userId, 0).getStatus(), taskStatus);
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final Task task = service.create(userId, taskName, taskDescription);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeTaskStatusById(null, task.getId(), taskStatus));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.changeTaskStatusById(userId, null, taskStatus));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeTaskStatusById(userId, task.getId(), null));
        service.changeTaskStatusById(userId, task.getId(), taskStatus);
        Assert.assertEquals(service.findOneById(userId, task.getId()).getStatus(), taskStatus);
    }


    @Test
    public void removeAll() {
        @Nullable final String emptyUser = null;
        Assert.assertEquals(0, service.getSize());
        service.create(userId, UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertEquals(1, service.getSize());
        service.create(userId, UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertEquals(2, service.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeAll(emptyUser));
        service.removeAll(userId);
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void existsById() {
        @NotNull final Task task = service.create(userId, taskName, taskDescription);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(null, task.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, null));
        Assert.assertTrue(service.existsById(task.getId()));
    }

    @Test
    public void findAll() {
        repository.create(userId, taskName);
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId,null));
        Assert.assertEquals(1, service.getSize());
        repository.create(userId, taskName);
        Assert.assertEquals(2, service.getSize());
        repository.create(userId, taskName);
        Assert.assertEquals(3, service.getSize());
        repository.create(userId, taskName);
        Assert.assertEquals(4, service.getSize());
    }

    @Test
    public void findAllByComparator() {
        final int taskLength = 10;
        for (int i = 0; i < taskLength; i++) {
            @NotNull final String taskName = String.format("Task_%d", taskLength - i - 1);
            service.create(userId, taskName);
        }
        @NotNull List<Task> tasks = service.findAll(userId, TaskSort.BY_NAME.getComparator());
        Assert.assertEquals(taskLength, tasks.size());
        for (int i = 0; i < taskLength; i++) {
            @NotNull final String taskName = String.format("Task_%d", i);
            Assert.assertEquals(taskName, tasks.get(i).getName());
        }
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Task task = service.create(userId, taskName);
        Assert.assertNotNull(service.findOneById(task.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(null, task.getId()));
        Assert.assertEquals(task.getId(), service.findOneById(userId, task.getId()).getId());
    }

    @Test
    public void findOneByIndex() {
        @NotNull final Task task = service.create(userId, taskName);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(null,0));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(userId, 1));
        Assert.assertEquals(task.getId(), service.findOneByIndex(userId, 0).getId());
    }

    @Test
    public void removeOneById() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Task task = service.create(userId, taskName);
        Assert.assertEquals(1, service.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeOneById(null, task.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeOneById(userId, null));
        service.removeOneById(userId, task.getId());
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void removeOneByIndex() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Task task = service.create(userId, taskName);
        Assert.assertEquals(1, service.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeOneByIndex(null, 0));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.removeOneByIndex(userId, 1));
        service.removeOneByIndex(userId, 0);
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void add() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Task task = new Task();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(null, task));
        @Nullable Task newTask = service.add(userId, task);
        Assert.assertNotNull(newTask);
        Assert.assertEquals(task, newTask);
        Assert.assertEquals(1, service.getSize());
    }

    @Test
    public void removeOne() {
        final int taskLenth = 10;
        for (int i = 0; i < taskLenth; i++) {
            @NotNull final String taskNameRandom = UUID.randomUUID().toString();
            service.create(userId, taskNameRandom);
        }
        Assert.assertEquals(taskLenth, service.getSize(userId));
        @NotNull final Task task = service.removeOne(service.findOneByIndex(userId, 0));
        Assert.assertEquals(taskLenth - 1, service.getSize(userId));
        Assert.assertFalse(service.existsById(task.getId()));
    }

    @Test
    public void getSize() {
        final int taskLenth = 10;
        for (int i = 0; i < taskLenth; i++) {
            @NotNull final String userIdRandom = UUID.randomUUID().toString();
            @NotNull final String taskNameRandom = UUID.randomUUID().toString();
            service.create(userIdRandom, taskNameRandom);
        }
        @NotNull final List<Task> tasks = service.findAll();
        Assert.assertEquals(taskLenth, tasks.size());
    }

}