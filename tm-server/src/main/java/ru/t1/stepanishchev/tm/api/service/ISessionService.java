package ru.t1.stepanishchev.tm.api.service;

import ru.t1.stepanishchev.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}